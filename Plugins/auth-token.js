'use strict';

let TokenManager = require('../Lib/TokenManager');
let UniversalFunctions = require('../Utils/UniversalFunctions');

exports.register = function(server, options, next){

//Register Authorization Plugin
    server.register(require('hapi-auth-bearer-token'), function (err) {
        server.auth.strategy('UserAuth', 'bearer-access-token', {

            allowQueryToken: false,
            allowMultipleHeaders: true,
            accessTokenName: 'accessToken',
            validateFunc: function (token, callback) {
                TokenManager.verifyToken(token,'USER', function (err,response) {
                    console.log("565656")
                    if (err || !response || !response.userData){
                        callback(null, false, {token: token, userData: null})
                    }else {
                        console.log("here in userauth success")
                        callback(null, true, {token: token, userData: response.userData})
                    }
                });
            }
        });
        server.auth.strategy('LibraryAuth', 'bearer-access-token', {

            allowQueryToken: false,
            allowMultipleHeaders: true,
            accessTokenName: 'accessToken',
            validateFunc: function (token, callback) {
                TokenManager.verifyToken(token,'LIBRARY', function (err,response) {
                    if (err || !response || !response.userData){
                        callback(null, false, {token: token, userData: null})
                    }else {
                        callback(null, true, {token: token, userData: response.userData})
                    }
                });

            }
        });

    });


    next();
};

exports.register.attributes = {
    name: 'auth-token-plugin'
};