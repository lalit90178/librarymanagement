'use strict';
let Hapi = require('hapi');
let Routes = require('./Routes');
const Inert = require('inert');
const Vision = require('vision');
const HapiSwagger = require('hapi-swagger');
const Pack = require('./package');
let mongoose = require('mongoose');
let Plugins = require('./Plugins');
mongoose.Promise = global.Promise;


let server = new Hapi.Server();

server.connection(
    { port:8000,
    routes: { cors: true },
});



mongoose.Promise = global.Promise;

mongoose.connect('mongodb://lalit:123456@ds163595.mlab.com:63595/library2');

const options = {
    info: {
        'title': 'LIBRARY API Documentation',
        'version': Pack.version
    }
};

server.register([
    Inert,
    Vision,
    {
        'register': HapiSwagger,
        'options': options
    }], function (err) {
    if (err){
        server.error('Error while loading plugins : ' + err)
    }else {
        server.log('info','Plugins Loaded')
    }
});

server.register(Plugins, function (err) {
    if (err){
        server.error('Error while loading plugins : ' + err)
    }else {
        server.log('info','Plugins Loaded')
    }
});

server.register(Inert, function(err){
    if(err){
        throw err;
    }
    server.route(Routes);
});



server.start(
    console.log('Server running at:', server.info.uri)
);

