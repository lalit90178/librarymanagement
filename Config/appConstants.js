
'use strict';

let SERVER = {
    APP_NAME: 'library',
    PORTS: {
        HAPI: 8001
    },
    JWT_SECRET_KEY: '1233',
};

let DATABASE = {


    USER_ROLES: {
        LIBRARY: 'LIBRARY',
        USER:'USER',

    },

    DEVICE_TYPES: {
        IOS: 'IOS',
        ANDROID: 'ANDROID'
    },

};

let STATUS_MSG = {
    ERROR: {

        BOOK_NOT_AVAILABLE: {
            statusCode:400,
            customMessage : 'Sorry the requested book is not available',
            type : 'BOOK_NOT_AVAILABLE'
        },

        ALREADY_EXIST: {
            statusCode:400,
            type: 'ALREADY_EXIST',
            customMessage : 'Already Exist '
        },

        DATA_NOT_FOUND: {
            statusCode:401,
            type: 'DATA_NOT_FOUND',
            customMessage : 'empty data'
        },


        INVALID_EMAIL_PASS: {
            statusCode:401,
            type: 'INVALID_EMAIL_PASS',
            customMessage : 'Invalid email or password'
        },

        TOKEN_ALREADY_EXPIRED: {
            statusCode:401,
            customMessage : 'Your login session expired!',
            type : 'TOKEN_ALREADY_EXPIRED'
        },

        DB_ERROR: {
            statusCode:400,
            customMessage : 'DB Error : ',
            type : 'DB_ERROR'
        },

        INVALID_ID: {
            statusCode:400,
            customMessage : 'Invalid Id Provided : ',
            type : 'INVALID_ID'
        },

        APP_ERROR: {
            statusCode:400,
            customMessage : 'Application Error',
            type : 'APP_ERROR'
        },

        ADDRESS_NOT_FOUND: {
            statusCode:400,
            customMessage : 'Address not found',
            type : 'ADDRESS_NOT_FOUND'
        },

        IMP_ERROR: {
            statusCode:500,
            customMessage : 'Implementation Error',
            type : 'IMP_ERROR'
        },


        INVALID_TOKEN: {
            statusCode:401,
            customMessage : 'Invalid token provided',
            type : 'INVALID_TOKEN'
        },


        DEFAULT: {
            statusCode:400,
            customMessage : 'Error',
            type : 'DEFAULT'
        },

        PHONE_NO_EXIST: {
            statusCode:400,
            customMessage : 'Phone No Already Exist',
            type : 'PHONE_NO_EXIST'
        },

        EMAIL_EXIST: {
            statusCode:400,
            customMessage : 'Email Already Exist',
            type : 'EMAIL_EXIST'
        },


        INVALID_EMAIL: {
            statusCode:400,
            customMessage : 'Invalid Email Address',
            type : 'INVALID_EMAIL'
        },

        INVALID_EMAIL_OR_PHONE_NUMBER: {
            statusCode:400,
            customMessage : 'Invalid Email Address or Phone Number',
            type : 'INVALID_EMAIL_OR_PHONE_NUMBER'
        },

        PASSWORD_REQUIRED: {
            statusCode:400,
            customMessage : 'Password is required',
            type : 'PASSWORD_REQUIRED'
        },


        PHONE_NO_REQUIRED: {
            statusCode:400,
            customMessage : 'Phone number is required',
            type : 'PHONE_NO_REQUIRED'
        },

        EMAIL_REQUIRED: {
            statusCode:400,
            customMessage : 'Email is required',
            type : 'EMAIL_REQUIRED'
        },


        FIRSTNAME_REQUIRED: {
            statusCode:400,
            customMessage : 'First Name is required',
            type : 'FIRSTNAME_REQUIRED'
        },



        NOT_FOUND: {
            statusCode:400,
            customMessage : 'User Not Found',
            type : 'NOT_FOUND'
        },


        EMAIL_ALREADY_EXIST: {
            statusCode:400,
            customMessage : 'Email Address Already Exists',
            type : 'EMAIL_ALREADY_EXIST'
        },


        PHONE_ALREADY_EXIST: {
            statusCode:400,
            customMessage : 'Phone number already exists',
            type : 'PHONE_ALREADY_EXIST'
        },

        EMAIL_NOT_REGISTERED: {
            statusCode:400,
            customMessage : 'Email Not Registered',
            type : 'EMAIL_NOT_REGISTERED'
        },
        BOOK_NOT_FOUND: {
            statusCode:400,
            customMessage : 'Book not fount',
            type : 'BOOK_NOT_FOUND'
        },


        PHONE_NOT_FOUND: {
            statusCode:400,
            customMessage : 'Phone No. Not Found',
            type : 'PHONE_NOT_FOUND'
        },

        UNAUTHORIZED: {
            statusCode:401,
            customMessage : 'You are not authorized to perform this action',
            type : 'UNAUTHORIZED'
        }
    },
    SUCCESS: {
        CREATED: {
            statusCode:201,
            customMessage : 'Created Successfully',
            type : 'CREATED'
        },
        DEFAULT: {
            statusCode:200,
            customMessage : 'Success',
            type : 'DEFAULT'
        },
        UPDATED: {
            statusCode:200,
            customMessage : 'Updated Successfully',
            type : 'UPDATED'
        },
        LOGOUT: {
            statusCode:200,
            customMessage : 'Logged Out Successfully',
            type : 'LOGOUT'
        },
        DELETED: {
            statusCode:200,
            customMessage : 'Deleted Successfully',
            type : 'DELETED'
        },
    }
};


let swaggerDefaultResponseMessages = [
    {code: 200, message: 'OK'},
    {code: 400, message: 'Bad Request'},
    {code: 401, message: 'Unauthorized'},
    {code: 404, message: 'Data Not Found'},
    {code: 500, message: 'Internal Server Error'}
];




let APP_CONSTANTS = {
    SERVER: SERVER,
    DATABASE: DATABASE,
    STATUS_MSG: STATUS_MSG,
    swaggerDefaultResponseMessages: swaggerDefaultResponseMessages
};

module.exports = APP_CONSTANTS;
