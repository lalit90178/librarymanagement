'use strict';
const Service = require('../Services');
const UniversalFunctions = require('../Utils/UniversalFunctions');
const async = require('async');
const TokenManager = require('../Lib/TokenManager');
const Config = require('../Config');


const registerLibrary = function (payloadData, callback) {
    let dataToInsert = {};
    let userData = {};
    let responseObject = {};
    async.series([
        function (cb) {
            //checkEmail in db
            let criteria = {
                email: payloadData.email,
            };

            let projection = {};
            let options = {
                lean: true
            };
            Service.libraryServices.getlibrary(criteria, projection, options, function (err, data) {
                if (err) {
                    cb(err);
                } else {
                    if (data && data.length) {
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_EXIST);
                    } else {
                        cb(null)
                    }
                }
            });
        },

        function (cb) {
            // insert library in db
            dataToInsert.name = payloadData.name;
            dataToInsert.address = payloadData.address;
            dataToInsert.email = payloadData.email;
            dataToInsert.password = UniversalFunctions.CryptData(payloadData.password);

            Service.libraryServices.createlibrary(dataToInsert, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result) {
                        userData = result;
                        cb(null)
                    } else {
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
                    }
                }
            })
        },

        function (cb) {
            // set access token
            if (userData) {
                let tokenData = {
                    id: userData._id,
                    type: 'LIBRARY'
                };

                TokenManager.setToken(tokenData, function (err, data) {
                    if (err) {
                        cb(err);
                    } else {
                        if (data) {
                            responseObject.accessToken = data.accessToken;
                            cb(null);
                        } else {
                            cb(null)
                        }
                    }
                })
            } else {
                cb(null)
            }
        },
    ], function (err, result) {
        callback(err, responseObject)
    })
};

const login = function (payloadData, callback) {
    let responseObject = {};
    let userData = {};
    let accessTokenUpdated = null;
    let password = UniversalFunctions.CryptData(payloadData.password);
    async.auto({
        
        checkUserExist: function ( cb) {
            let criteria = {
                email: payloadData.email,
                password:password,
            };
            let projections = {};
            let options = {
                lean: true
            };
            Service.libraryServices.getlibrary(criteria, projections, options, function (err, data) {
                if (err) {
                    cb(err);
                } else {
                    if (data && data.length) {
                        userData = data;
                        cb(null)
                    } else {
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_EMAIL_PASS);
                    }
                }
            })
        },

        generateTokenAndUpdate: ['checkUserExist', function (err, cb) {
            let tokenData = {
                id: userData[0]._id,
                type: 'LIBRARY'
            };
            TokenManager.setToken(tokenData, function (err, data) {
                if (err) {
                    cb(err);
                } else {
                    accessTokenUpdated = data.accessToken;
                    responseObject.accessToken=data.accessToken
                    cb(null);
                }
            })
        }],

    }, function (err, result) {
        callback(err, responseObject)
    })
};

const addBooks = function (payloadData, userData, callback) {
    let responseObject = {};
    async.auto({
        createBook:function (cb) {
            let dataToSave = {
                name:payloadData.name,
                publication:payloadData.name,
                uniqueBookId:payloadData.uniqueBookId,
                libraryId:userData._id,

            };
            Service.bookServices.createBook(dataToSave,function (err, result) {
                if(err){
                    cb(err)
                }else{
                    responseObject=result;
                    cb(null)
                }
            })
        }

    },function (err, result) {
        callback(err,responseObject)
    })
};


module.exports = {
    registerLibrary: registerLibrary,
    login:login,
    addBooks:addBooks,
};