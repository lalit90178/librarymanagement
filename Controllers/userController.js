'use strict';
const Service = require('../Services');
const UniversalFunctions = require('../Utils/UniversalFunctions');
const async = require('async');
const TokenManager = require('../Lib/TokenManager');
const Config = require('../Config');


const registerUser = function (payloadData, callback) {
    let dataToInsert = {};
    let userData = {};
    let responseObject = {};
    async.series([
        function (cb) {
            //checkEmail in db
            let criteria = {
                email: payloadData.email,
                isDeleted: false,
            };

            let projection = {};
            let options = {
                lean: true
            };
            Service.userServices.getUsers(criteria, projection, options, function (err, data) {
                if (err) {
                    cb(err);
                } else {
                    if (data && data.length) {
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_EXIST);
                    } else {
                        cb(null)
                    }
                }
            });
        },

        function (cb) {
            // insert user in db
            dataToInsert.name = payloadData.name;
            dataToInsert.email = payloadData.email;
            dataToInsert.password = UniversalFunctions.CryptData(payloadData.password);

            Service.userServices.createUsers(dataToInsert, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result) {
                        userData = result;
                        cb(null)
                    } else {
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
                    }
                }
            })
        },

        function (cb) {
            // set access token
            if (userData) {
                let tokenData = {
                    id: userData._id,
                    type: 'USER'
                };
                TokenManager.setToken(tokenData, function (err, data) {
                    if (err) {
                        cb(err);
                    } else {
                        if (data) {
                            responseObject.accessToken = data.accessToken;
                            cb(null);
                        } else {
                            cb(null)
                        }
                    }
                })
            } else {
                cb(null)
            }
        },
    ], function (err, result) {
        callback(err, responseObject)
    })
};

const userLogin = function (payloadData, callback) {
    let responseObject = {};
    let userData = {};
    let accessTokenUpdated = null;
    let password = UniversalFunctions.CryptData(payloadData.password);
    async.auto({

        checkUserExist:  function (cb) {
            let criteria = {
                email: payloadData.email,
                password:password,
                isDeleted: false,
            };
            let projections = {};
            let options = {
                lean: true
            };
            Service.userServices.getUsers(criteria, projections, options, function (err, data) {
                if (err) {
                    cb(err);
                } else {
                    if (data && data.length) {
                        userData = data;
                        cb(null)
                    } else {
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_NOT_REGISTERED);
                    }
                }
            })
        },

        generateTokenAndUpdate: ['checkUserExist', function (err, cb) {
            let tokenData = {
                id: userData[0]._id,
                type: 'USER'
            };
            TokenManager.setToken(tokenData, function (err, data) {
                if (err) {
                    cb(err);
                } else {
                    accessTokenUpdated = data.accessToken;
                    responseObject.accessToken=data.accessToken
                    cb(null);
                }
            })
        }],
    }, function (err, result) {
        callback(err, responseObject)
    })
};

const getAvailableBooks = function (payloadData, userData, callback) {
    let responseObject =[];
    async.auto({
        getBooks : function (cb) {
            let criteria={
                isIssued:false,
            };
            let projection={};
            let option={lean:true};
            Service.bookServices.getBook(criteria,projection,option,function (err, result) {
                if(err){
                    cb(err)
                }else{
                    if(result && result.length){
                        responseObject=result;
                        cb(null)
                    }else{
                        cb(null)
                    }
                }
            })
        }
    },function (err, result) {
        callback(err,responseObject)
    })
};

const issueBook = function (payloadData, userData, callback) {
    let responseObject =[];
    let issueDetails=[];
    async.auto({
        getBooks : function (cb) {
            let criteria={
                _id:payloadData.bookId,
                isIssued:false,
                currentIssuerId:{$nin:[userData._id]}
            };
            let projection={};
            let option={lean:true};
            Service.bookServices.getBook(criteria,projection,option,function (err, result) {
                if(err){
                    cb(err)
                }else{

                    if(result && result.length){
                        responseObject=result;
                        issueDetails=result[0].issueDetails;
                        cb(null)
                    }else{
                        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.BOOK_NOT_FOUND);
                    }
                }
            })
        },

        issueBook:['getBooks', function (err, cb) {
            if(responseObject && responseObject.length) {
                // console.log("________________date",new Date(payloadData.issuedTill))
                let dataToPush={
                    issuer:userData._id,
                    isssuedOn:Date.now(),
                    issuedTill:new Date(payloadData.issuedTill),
                };
                issueDetails.push(dataToPush);
                let criteria={
                    _id:payloadData.bookId,
                };
                let dataToUpdate={
                    isssuedOn:Date.now(),
                    issuedTill:new Date(payloadData.issuedTill),
                    isIssued:true,
                    issueDetails:issueDetails,
                    currentIssuerId:userData._id

                };
                let option={new:true};
                Service.bookServices.updateBook(criteria,dataToUpdate,option,function (err, result) {
                    if(err){
                        cb(err)
                    }else{
                        if(result && result.length){
                            responseObject=result;
                            cb(null)
                        }else{
                            cb(null)
                        }
                    }
                })
            }else{
                cb(null)
            }
        }],

        issueBookUser:['getBooks', function (err, cb) {
            if(responseObject && responseObject.length) {
                let criteria={
                    _id:userData._id,
                };
                let projection={
                    $push:{
                        issuedBook:payloadData.bookId
                    }
                };
                let option={new:true};
                Service.userServices.updateUsers(criteria,projection,option,function (err, result) {
                    if(err){
                        cb(err)
                    }else{
                        if(result && result.length){
                            cb(null)
                        }else{
                            cb(null)
                        }
                    }
                })
            }else{
                cb(null)
            }
        }]

    },function (err, result) {
        callback(err,responseObject)
    })
};

const getAUserIssuedBooks = function (payloadData, userData, callback) {
    let responseObject =[];
    async.auto({
        getBooks : function (cb) {
            let criteria={
                currentIssuerId:userData._id
            };
            let projection={
                _id:1,
                name:1,
                publication:1,
                uniqueBookId:1,
                currentIssuerId:1,
                libraryId:1,
                isssuedOn:1,
                issuedTill:1
            };
            let option={lean:true};
            Service.bookServices.getBook(criteria,projection,option,function (err, result) {
                if(err){
                    cb(err)
                }else{
                    if(result && result.length){
                        responseObject=result;
                        cb(null)
                    }else{
                        cb(null)
                    }
                }
            })
        }
    },function (err, result) {
        callback(err,responseObject)
    })
};

const returnBook = function (payloadData, userData, callback) {
    let responseObject =[];
    let issueDetails=[];
    async.auto({
        getBooks : function (cb) {
            let criteria={
                _id:payloadData.bookId,
            };
            let projection={};
            let option={lean:true};
            Service.bookServices.getBook(criteria,projection,option,function (err, result) {
                if(err){
                    cb(err)
                }else{
                    if(result && result.length){
                        responseObject=result;
                        issueDetails=result[0].issueDetails
                        cb(null)
                    }else{
                        cb(null)
                    }
                }
            })
        },

        changeIssueDetails:['getBooks', function (err, cb) {
            issueDetails.map(function (obj) {
                if((obj.issuer).toString() == (userData._id).toString()){
                    obj.returnedOn=new Date();
                }else{

                }
            })
            cb(null)
        }],

        removeFromBook:['changeIssueDetails',function (err, cb) {
            if(responseObject && responseObject.length) {
                let criteria={
                    _id:payloadData.bookId,
                };
                let projection={
                    $unset:{
                        currentIssuerId:1,
                        issuedTill:1,
                        isssuedOn:1,
                    },
                    issueDetails:issueDetails,
                    isIssued:false,
                };
                let option={lean:true};
                Service.bookServices.updateBook(criteria,projection,option,function (err, result) {
                    if(err){
                        cb(err)
                    }else{
                        if(result && result.length){
                            cb(null)
                        }else{
                            cb(null)
                        }
                    }
                })
            }else{
                cb(null)
            }
        }],

        removeFromUser:['changeIssueDetails',function (err, cb) {
            if(responseObject && responseObject.length) {
                let criteria={
                    _id:userData._id,
                };
                let projection={
                    $pull:{
                        issuedBook:payloadData.bookId,
                    }
                };
                let option={lean:true};
                Service.userServices.updateUsers(criteria,projection,option,function (err, result) {
                    if(err){
                        cb(err)
                    }else{
                        if(result && result.length){
                            cb(null)
                        }else{
                            cb(null)
                        }
                    }
                })
            }else{
                cb(null)
            }
        }]

    },function (err, result) {
        callback(err,responseObject)
    })
};


module.exports = {

    registerUser: registerUser,
    userLogin:userLogin,
    getAvailableBooks:getAvailableBooks,
    issueBook:issueBook,
    getAUserIssuedBooks:getAUserIssuedBooks,
    returnBook:returnBook

};