let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let Config = require('../Config');

let library = new Schema({
    name: {type: String, trim: true, index: true, unique:true, sparse:true, default: ''},
    registrationDate: {type: Date, default: Date.now, required: true},
    address: {type: String, trim: true, index: true,sparse: true},
    email: {type: String, trim: true, index: true,sparse: true},
    password: {type: String,default:''},

    accessToken: {type: String, trim: true, index: true},
});



module.exports = mongoose.model('library', library);
