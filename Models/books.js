let mongoose = require('mongoose');
let Schema = mongoose.Schema;


let issueDetails = new Schema({
    issuer: {type: Schema.ObjectId, ref: "Users", default: null},
    isssuedOn:{type:Date,default:null},
    issuedTill:{type:Date,default:null},
    returnedOn:{type:Date,default:null}
});



let books = new Schema({
    name:{type: String,default:"",trim:true},
    publication:{type: String,default:"",trim:true},
    uniqueBookId:{type: String, trim: true, index: true, unique:true, sparse:true, default: ''},
    libraryId:{type: Schema.ObjectId, ref: "library", default: null},

    currentIssuerId:{type: Schema.ObjectId, ref: "user", default: null},
    isssuedOn:{type:Date,default:null},
    issuedTill:{type:Date,default:null},
    isIssued:{type:Boolean,default:false,required:true},
    issueDetails:{type:[issueDetails], default:[]}
});



module.exports = mongoose.model('books', books);