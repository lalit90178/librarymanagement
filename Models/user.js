let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let Users = new Schema({
    name: {type: String, trim: true, index: true, default: '', sparse: true},
    email: {type: String, trim: true, index: true,sparse: true},
    password: {type: String,default:''},
    accessToken: {type: String, trim: true, index: true},
    registrationDate: {type: Date, default: Date.now, required: true},
    isDeleted: {type: Boolean, default: false, required: true},

    issuedBook:{type: [Schema.ObjectId], ref: "books", default: null},
});



module.exports = mongoose.model('Users', Users);
