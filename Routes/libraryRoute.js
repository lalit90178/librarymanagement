'use strict';
let Controller = require('../Controllers');
let UniversalFunctions = require('../Utils/UniversalFunctions');
let Joi = require('joi');
let Config = require('../Config');


let libraryRoute = [
    {
        method: 'POST',
        path: '/library/registerLibrary',
        handler: function (request, reply) {
            let payloadData = request.payload;
            Controller.libraryController.registerLibrary(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'add a new library',
            tags: ['api', 'library'],
            validate: {
                payload: {
                    name:Joi.string().required(),
                    address:Joi.string().required(),
                    email:Joi.string().required(),
                    password:Joi.string().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/library/login',
        handler: function (request, reply) {
            let payloadData = request.payload;
            Controller.libraryController.login(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'library login',
            tags: ['api', 'library'],
            validate: {
                payload: {
                    email:Joi.string().required().email(),
                    password:Joi.string().required(),
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/library/addBooks',
        handler: function (request, reply) {
            let userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                let payloadData = request.payload;
                Controller.libraryController.addBooks(payloadData, userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                });

        },
        config: {
            description: 'Add books to the library',
            tags: ['api', 'library'],
            auth: 'LibraryAuth',
            validate: {
                payload: {
                    name:Joi.string().required(),
                    publication:Joi.string().required(),
                    uniqueBookId:Joi.string().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
];



module.exports = libraryRoute;
