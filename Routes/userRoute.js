'use strict';
let Controller = require('../Controllers');
let UniversalFunctions = require('../Utils/UniversalFunctions');
let Joi = require('joi');
let Config = require('../Config');


let userRoute = [
    {
        method: 'POST',
        path: '/user/registerUser',
        handler: function (request, reply) {
            let payloadData = request.payload;
            Controller.userController.registerUser(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'add a new user',
            tags: ['api', 'users'],
            validate: {
                payload: {
                    name:Joi.string().required(),
                    email:Joi.string().required().email(),
                    password:Joi.string().required(),
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/user/userLogin',
        handler: function (request, reply) {
            let payloadData = request.payload;
            Controller.userController.userLogin(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'user login',
            tags: ['api', 'users'],
            validate: {
                payload: {
                    email:Joi.string().required().email(),
                    password:Joi.string().required(),
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/user/getAvailableBooks',
        handler: function (request, reply) {
            let userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                let payloadData = request.query;
                Controller.userController.getAvailableBooks(payloadData, userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                });


        }, config: {
        description: 'get all Available Books',
        auth: 'UserAuth',
        tags: ['api', 'user'],
        validate: {
            query: {
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'POST',
        path: '/user/issueBook',
        handler: function (request, reply) {

            let userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                let payloadData = request.payload;
                Controller.userController.issueBook(payloadData, userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                });

        },
        config: {
            description: 'Issue Book (dd/mm/yyyy)',
            tags: ['api', 'users'],
            auth: 'UserAuth',
            validate: {
                payload: {
                    bookId:Joi.string().required(),
                    issuedTill:Joi.string().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/user/getAUserIssuedBooks',
        handler: function (request, reply) {
            let userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                let payloadData = request.query;
                Controller.userController.getAUserIssuedBooks(payloadData, userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                });


        }, config: {
        description: 'get Users Issued Books',
        auth: 'UserAuth',
        tags: ['api', 'user'],
        validate: {
            query: {
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },

    {
        method: 'POST',
        path: '/user/returnBook',
        handler: function (request, reply) {

            let userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
                let payloadData = request.payload;
                Controller.userController.returnBook(payloadData, userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                });

        },
        config: {
            description: 'Return Book',
            tags: ['api', 'users'],
            auth: 'UserAuth',
            validate: {
                payload: {
                    bookId:Joi.string().required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

];



module.exports = userRoute;
