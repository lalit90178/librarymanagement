'use strict';

let Config = require('../Config');
let Jwt = require('jsonwebtoken');
let async = require('async');
let Service = require('../Services');


let getTokenFromDB = function (userId, userType,flag,token, callback) {
    let userData = null;


    let criteria = {
        _id: userId,
        accessToken :{$in:[token]}

    };
    async.series([
        function (cb) {

            if (userType == Config.APP_CONSTANTS.DATABASE.USER_ROLES.USER && flag=='USER'){
                Service.userServices.getUsers(criteria,{},{lean:true}, function (err, dataAry) {
                    if (err){
                        cb(err)
                    }else {
                        if (dataAry && dataAry.length > 0){
                            userData = dataAry[0];
                            console.log("in token check ")
                            //userData.userType=userType;
                            cb();
                        }else {
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN)
                        }
                    }

                });

            }
            else if (userType == Config.APP_CONSTANTS.DATABASE.USER_ROLES.LIBRARY  && flag=='LIBRARY'){
                Service.libraryServices.getlibrary(criteria,{},{lean:true}, function (err, dataAry) {
                    if (err){
                        cb(err)
                    }else {
                        if (dataAry && dataAry.length > 0){
                            userData = dataAry[0];
                            cb();
                        }else {
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN)
                        }
                    }

                });

            }

            else {
                cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }
        }
    ], function (err, result) {
        if (err){
            callback(err)
        }else {
            if (userData && userData._id){
                userData.id = userData._id;
                userData.type = userType;
            }
            callback(null,{userData: userData})
        }
    });
};

let setTokenInDB = function (userId,userType, tokenToSave, callback) {
    let criteria = {
        _id: userId
    };

    let setQuery = {
            accessToken : tokenToSave

    };
    async.series([
        function (cb) {
             if (userType == Config.APP_CONSTANTS.DATABASE.USER_ROLES.USER){
                Service.userServices.updateUsers(criteria,setQuery,{new:true}, function (err, dataAry) {
                    if (err){
                        cb(err)
                    }else {
                        if (dataAry && dataAry._id){
                            cb();
                        }else {
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                        }
                    }
                });

            }
            else if (userType == Config.APP_CONSTANTS.DATABASE.USER_ROLES.LIBRARY){
                Service.libraryServices.updatelibrary(criteria,setQuery,{new:true}, function (err, dataAry) {
                    if (err){
                        cb(err)
                    }else {
                        if (dataAry && dataAry._id){
                            cb();
                        }else {
                            cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                        }
                    }

                });

            }
            else {
                cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }
        }
    ], function (err, result) {
        if (err){
            callback(err)
        }else {
            callback()
        }
    });
};

let expireTokenInDB = function (userId,userType, callback) {
    let criteria = {
        _id: userId
    };
    let setQuery = {
        accessToken : null
    };
    async.series([
        function (cb) {
             if (userType == Config.APP_CONSTANTS.DATABASE.USER_ROLES.LIBRARY){
                Service.libraryServices.updatelibrary(criteria,setQuery,{new:true}, function (err, dataAry) {
                    if (err){
                        callback(err)
                    }else {
                        if (dataAry && dataAry.length > 0){
                            cb();
                        }else {
                            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN)
                        }
                    }

                });
            }else {
                cb(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }
        }
    ], function (err, result) {
        if (err){
            callback(err)
        }else {
            callback()
        }

    });
};


let  verifyToken = function (token,flag, callback) {
    let response = {
        valid: false
    };
    Jwt.verify(token, Config.APP_CONSTANTS.SERVER.JWT_SECRET_KEY, function (err, decoded) {
        if (err) {

            callback(err)
        } else {

            getTokenFromDB(decoded.id, decoded.type,flag,token, callback);
        }
    });
};

let setToken = function (tokenData, callback) {
    if (!tokenData.id || !tokenData.type) {
        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        let tokenToSend = Jwt.sign(tokenData, Config.APP_CONSTANTS.SERVER.JWT_SECRET_KEY);
       // console.log('token>>>>',err,data,tokenData.type)
        setTokenInDB(tokenData.id,tokenData.type, tokenToSend, function (err, data) {
            callback(err, {accessToken: tokenToSend})
        })
    }
};

let expireToken = function (token, callback) {
    Jwt.verify(token, Config.APP_CONSTANTS.SERVER.JWT_SECRET_KEY, function (err, decoded) {
        if (err) {
            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
        } else {
            expireTokenInDB(decoded.id,decoded.type, function (err, data) {
                callback(err, data)
            });
        }
    });
};

let decodeToken = function (token, callback) {
    Jwt.verify(token, Config.APP_CONSTANTS.SERVER.JWT_SECRET_KEY, function (err, decodedData) {
        if (err) {
            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
        } else {
            callback(null, decodedData)
        }
    })
};

module.exports = {
    expireToken: expireToken,
    setToken: setToken,
    verifyToken: verifyToken,
    decodeToken: decodeToken
};