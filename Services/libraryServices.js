'use strict';

let Models = require('../Models');

let getlibrary= function (criteria, projection, options, callback) {
    Models.library.find(criteria, projection, options, callback);
};

let createlibrary= function (objToSave, callback) {
    new Models.library(objToSave).save(callback)
};

let updatelibrary= function (criteria, dataToSet, options, callback) {
    Models.library.findOneAndUpdate(criteria, dataToSet, options, callback);
};


module.exports = {
    getlibrary: getlibrary,
    createlibrary: createlibrary,
    updatelibrary: updatelibrary,
};

