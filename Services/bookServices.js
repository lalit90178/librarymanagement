'use strict';

let Models = require('../Models');

let getBook= function (criteria, projection, options, callback) {
    Models.books.find(criteria, projection, options, callback);
};

let createBook= function (objToSave, callback) {
    new Models.books(objToSave).save(callback)
};

let updateBook= function (criteria, dataToSet, options, callback) {
    Models.books.findOneAndUpdate(criteria, dataToSet, options, callback);
};


module.exports = {
    getBook: getBook,
    createBook: createBook,
    updateBook: updateBook,
};

