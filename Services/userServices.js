'use strict';

let Models = require('../Models');

let getUsers= function (criteria, projection, options, callback) {
    Models.user.find(criteria, projection, options, callback);
};

let createUsers= function (objToSave, callback) {
    new Models.user(objToSave).save(callback)
};

let updateUsers= function (criteria, dataToSet, options, callback) {
    Models.user.findOneAndUpdate(criteria, dataToSet, options, callback);
};


module.exports = {
    getUsers: getUsers,
    createUsers: createUsers,
    updateUsers: updateUsers,
};

